import {useState} from 'react';
import Head from 'next/head'
import Image from 'next/image'
import { AnimatePresence } from "framer-motion";

import Slide from './components/Slide';
import styles from '../styles/Home.module.css'
import Image1 from '../assets/image1.svg';


export default function Home() {
  const [active, setActive] = useState(1);
  const [data] = useState([1, 2, 3]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Home Screen</title>
        <meta name="description" content="home screen" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet" />
      </Head>

      <main className={`min-w-full max-w-sm mx-auto flex flex-col h-screen`}>
        <header className={`${styles.header}`}>
          <p className={`flex items-center justify-end pr-5`}>Login</p>
        </header>
        <section className="flex flex-col">
          <section>
            <h1 className={styles.title}>Deine Job website</h1>
            <Image src={Image1} className={styles.slideArea} alt="image1" />
          </section>
          <section className={`flex flex-col bg-white`}>
            <ul className={`flex overflow-x-scroll px-2 ${styles.buttonGroup}`}>
              <li><button className={active === 1 ? styles.buttonActive : styles.button} onClick={() => setActive(1)}>Arbeinehmer</button></li>
              <li><button className={active === 2 ? styles.buttonActive : styles.button} onClick={() => setActive(2)}>Arbeigeber</button></li>
              <li><button className={active === 3 ? styles.buttonActive : styles.button} onClick={() => setActive(3)}>Temporaburo</button></li>
            </ul>
            <section className="mb-32">
              <h2 className={styles.subTitle}>Drei einfache Schritte zu deinem neuen Job</h2>
              <AnimatePresence>
                {data.map((item, index) => <Slide key={`item-${item}-${index}`} item={item} active={active} />)}
              </AnimatePresence>
            </section>
          </section>
        </section>
        <footer className={`fixed bottom-0 left-0 right-0 flex jutify-center ${styles.footerContainer} p-4`}>
          <button className={`mx-0.5 py-3 text-white ${styles.buttonArea}`}>Kostenlos Registrieren</button>
        </footer>
      </main>
    </div>
  )
}
