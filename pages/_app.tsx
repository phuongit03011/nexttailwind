import '../styles/globals.css';
import type { AppProps } from 'next/app';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faMicrophone, faPlus, faUserCircle } from '@fortawesome/free-solid-svg-icons';

library.add(fab, faMicrophone, faPlus, faUserCircle);

function MyApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
export default MyApp
