import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import HeaderBlue from '../components/Header/HeaderBlue';
import OnboardingModal from '../components/OnboardingModal';
import styles from '../../styles/Header.module.css';

const Ask = () => {
  const [showModal, setShowModal] = React.useState(false);

  const onShowModal = () => {
    setShowModal(!showModal);
  }

  return (
    <div className={`max-w-sm mx-auto flex flex-col h-full ${styles.appContainer}`}>
      <div className={styles.headerContainer}>
        <div className={`${styles.userInfoContainer}`}>
          <FontAwesomeIcon icon="user-circle" className="mr-5" size="4x" color="#FFFFFF" />
          <h3 className="text-white">Kelly Choo</h3>
        </div>
        <HeaderBlue />
        <p className={`text-white ${styles.header}`}>Introduce yourself to Kelly</p>
      </div>
      <div className={`flex flex-col p-5 overflow-x-hidden overflow-y-scroll`}>
        <div className="bg-gray-300 p-5 rounded mb-5">
          <h3 className={`${styles.title} mb-5`}>KELLY’S INTRO</h3>
          <p className="text-sm">
            As a founder, I have a dream that anyone 
            can use ReferReach to connect with relevant people to solve their challenges. 
          </p>
        </div>
        <div className="bg-gray-300 p-5 rounded mb-10">
          <h3 className={`${styles.title} mb-5`}>KELLY’S INTRO</h3>
          <p className="text-sm">
            As a founder, I have a dream that anyone 
            can use ReferReach to connect with relevant people to solve their challenges. 
          </p>
        </div>
      </div>
      <div className={`max-w-sm fixed bottom-0 w-full bg-white`}>
        <div className="flex border-t border-gray-300 p-2 items-center">
          <FontAwesomeIcon icon="plus" className="mr-5" />
          <div className="mr-5 flex-1" onClick={onShowModal}>
            <input disabled className="appearance-none block w-full bg-gray-300 rounded" type="text" />
          </div>
          <FontAwesomeIcon icon="microphone" />
        </div>
      </div>
      <OnboardingModal onShowModal={onShowModal} showModal={showModal} />
    </div>
  )
}

export default Ask;