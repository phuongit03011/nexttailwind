import Modal from 'react-modal';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

import styles from '../../../styles/Modal.module.css';

interface IFormInputs {
  fullName: string;
  email: string;
  how_i_help: string;
}

interface Props {
  showModal: boolean;
  onShowModal: () => void;
}

const schema = yup.object().shape({
  fullName: yup.string().required(),
  email: yup.string().email('Invalid email format').required(),
  how_i_help: yup.string().required(),
});

const OnboardingModal: React.FC<Props> = ({showModal = false, onShowModal}: Props) => {
  const { register, handleSubmit, watch, formState: { errors, isValid } } = useForm<IFormInputs>({
    resolver: yupResolver(schema),
    mode: "onChange"
  });

  const onSubmitHandler = (data: any) => {
    console.log({ data });
  };
console.log(errors);
console.log(isValid);
  return (
    <Modal
      closeTimeoutMS={200}
      isOpen={showModal}
      contentLabel="modal"
      onRequestClose={() => onShowModal()}
      className={`max-w-sm mx-auto flex flex-col h-full border-0 justify-center items-center p-5 ${styles.ReactModal__Overlay}`}
    >
      <form onSubmit={handleSubmit(onSubmitHandler)} className="w-full">
        <div className="h-full bg-white w-full p-5 rounded">
          <h2 className={`${styles.textPrimary} text-center mb-10`}>REFERREACH</h2>
          <div className="mb-5 flex flex-col">
            <label className="text-black mb-1">Full name</label>
            <input {...register("fullName")} className={`border border-black outline-none rounded-xl p-2 ${styles.textPrimary}`} />
            {errors.fullName && <span className="text-red-500">{errors?.fullName?.message}</span>}
          </div>
          <div className="mb-5 flex flex-col">
            <label className="text-black mb-1">Email</label>
            <input {...register("email")} className={`border border-black outline-none rounded-xl p-2 mb-2 ${styles.textPrimary}`} />
            {errors.email && <span className="text-red-500">{errors?.email?.message}</span>}
            <p className="text-xs text-gray-500">We’ll notify you via email if Kelly is interested to chat.</p>
          </div>
          <div className="mb-5 flex flex-col">
            <label className="text-black mb-1">How can you help Kelly?</label>
            <textarea {...register("how_i_help")} className={`border border-black outline-none rounded-xl w-full p-2 ${styles.textPrimary}`} rows={5} />
              <div className="text-right my-2">{`${watch('how_i_help')?.length || 0} / 300 characters`}</div>
              {errors?.how_i_help && <span className="text-red-500">{errors?.how_i_help.message}</span>}
          </div>
          <div className="text-center">
            <button type="submit" disabled={!isValid} className={`rounded-full text-white px-6 py-2 ${isValid ? styles.backgroundGreen : styles.backgroundGray}`}>Submit</button>
          </div>
        </div>
      </form>
    </Modal>
  );
};

export default OnboardingModal;
