import {useState} from 'react';
import Image from 'next/image';
import { motion, AnimatePresence } from "framer-motion";

import styles from '../../../styles/Home.module.css';
import Image2 from '../../../assets/image2.svg';
import Image3 from '../../../assets/image3.svg';
import Image4 from '../../../assets/image4.svg';

const variants = {
  visible: {
    opacity: 1,
    display: "block",
    transition: { duration: 0.5, ease: "easeInOut" },
  },
  hidden: {
    opacity: 0,
    transitionEnd: {
      display: "none",
    },
    transition: { duration: 0.5, ease: "easeInOut" },
  },
}

const Slide = ({item, active}: any) => {

  return (
    <motion.section 
      variants={variants}
      animate={active === item ? "visible" : "hidden"}
      className={`${styles.sliderContainer}`}
    >
      <article className={`flex items-end ${styles.block}`}>
        <h1 className={`${styles.titleSlide} ${styles.titleSlide1} text-center`}>1.</h1>
        <div className="flex flex-col">
          <Image className={`${styles.imgArea} mb-8`} src={Image2} alt="image2" />
          <p className={styles.description}>Erstellen dein Lebenslauf</p>
        </div>
      </article>
      <article className={`flex flex-col ${styles.curve} ${styles.block}`}>
        <div className="flex flex-row items-end mb-8">
          <h1 className={`${styles.titleSlide} ${styles.titleSlide2} text-center`}>2.</h1>
          <p className={styles.description}>Erstellen dein Lebenslauf</p>
        </div>
        <Image className={styles.imgArea} src={Image3} alt="image3" />
      </article>
      <article className={`flex flex-col ${styles.curveWhite} ${styles.block}`}>
        <div className="flex flex-row items-end mb-8">
          <h1 className={`${styles.titleSlide} ${styles.titleSlide3} text-center`}>3.</h1>
          <p className={`${styles.description} ${styles.description3}`}>Mit nur einem Klick Bewerben</p>
        </div>
        <Image className={styles.imgArea} src={Image4} alt="image4" />
      </article>
    </motion.section>
  )
}

export default Slide;