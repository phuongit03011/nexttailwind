import * as React from "react";

const HeaderBlue = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 394 194"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M93.634 141.653C19.99 141.653.526 176.551 0 194V0h394v87.997c0 45.28-59.617 54.637-89.426 53.656H93.634z"
        fill="#3D75C9"
      />
    </svg>
  );
}

export default HeaderBlue;